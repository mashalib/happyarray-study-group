﻿using System;

namespace HW_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter roman number");

            string input = Console.ReadLine().ToLower();

            Console.WriteLine(ConvertToInteger(input));
            static int ConvertToInteger(string input)
            {
                int result = 0;
                foreach (var i in input)
                {
                    result = i switch
                    {
                        'i' => result + 1,
                        'v' => result + 5,
                        'x' => result + 10,
                        'l' => result + 50,
                        'c' => result + 100,
                        'd' => result + 500,
                        'm' => result + 1000,
                    //_=> 0 can skip this step, because
                    //From the description: input contains only the characters ('I', 'V', 'X', 'L', 'C', 'D', 'M').
                    //It is guaranteed that input valid roman numeral in the range[1, 3999]
                    };

                }
                if (input.Contains("iv") || input.Contains("ix"))
                {
                    result -= 2;
                }
                if (input.Contains("xl") || input.Contains("xc"))
                {
                    result -= 20;
                }
                if (input.Contains("cd") || input.Contains("cm"))
                {
                    result -= 200;
                }

                return result;
            }
            //static int ConvertToInteger(string input)
            //{
            //    int result = 0;
            //    foreach (var i in input)
            //    {
            //        switch (i)
            //        {
            //            case 'i':
            //                result += 1;
            //                break;
            //            case 'v':
            //                result += 5;
            //                break;
            //            case 'x':
            //                result += 10;
            //                break;
            //            case 'l':
            //                result += 50;
            //                break;
            //            case 'c':
            //                result += 100;
            //                break;
            //            case 'd':
            //                result += 500;
            //                break;
            //            case 'm':
            //                result += 1000;
            //                break;

            //            default:
            //                Console.WriteLine("you entered incorrect symbol(s)");
            //                return 0;
            //        }


            //    }
            //    if (input.Contains("iv") || input.Contains("ix"))
            //    {
            //        result -= 2;
            //    }
            //    if (input.Contains("xl") || input.Contains("xc"))
            //    {
            //        result -= 20;
            //    }
            //    if (input.Contains("cd") || input.Contains("cm"))
            //    {
            //        result -= 200;
            //    }

            //    return result;
            //}
        }
    }
}
