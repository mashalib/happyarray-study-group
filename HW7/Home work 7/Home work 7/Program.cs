﻿using System;
using System.Linq;

namespace Home_work_7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter numbers");
            string input = (Console.ReadLine());
            string[] numbersString = input.Split(',');
            int[] numbers = Array.ConvertAll(numbersString, s => int.Parse(s));
            int min = numbers.Min();
            int max = min;
            for (int i = (Array.IndexOf(numbers,min)+1); i < numbers.Length; i++)
            {
                if (numbers[i]>max)
                {
                    max = numbers[i];
                }
            }
            Console.WriteLine(max-min);
        }
    }
}
