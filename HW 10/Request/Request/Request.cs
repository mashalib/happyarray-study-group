﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Request
{
    public class Request
    {
        public Request(int requestId, string licenseType, int status, int facilityId, DateTime dateCreated)
        {
            RequestId = requestId;
            LicenseType = licenseType;
            Status = status;
            FacilityID = facilityId;
            DateCreated = dateCreated;
        }
        public int RequestId { get; set; }
        public string LicenseType { get; set; }
        public int Status { get; set; }
        public int FacilityID { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
