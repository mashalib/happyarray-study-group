﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Request
{
    class Program
    {
        static void Main(string[] args)
        {
            var filePath = @"https://drive.google.com/file/d/1stm4QwBV_OIiQY5A6twAWhu7d_6-aOLC/view?usp=sharing";

            var requestList = new List<Request>();

            using (var file = new StreamReader(filePath))
            {
                string? line;
                var counter = 1;
                while ((line = file.ReadLine()) != null)
                {
                    if (counter == 1)
                    {
                        counter++;
                        continue;
                    }

                    var request = Map(line);
                    requestList.Add(request);
                }
            }

            var licenseType = requestList.GroupBy(p => p.LicenseType).Select(s=>s.First()).ToArray();
            var countByType = requestList.GroupBy(p=>p.LicenseType).ToDictionary(p=>p.First().ToString(), p=>p.Count());
            var status2In2021 = requestList.Where((p => p.Status==2 && p.DateCreated.Year == 2021));
          
        }

        static Request Map(string line)
        {
            var LineArray = line.Split(',');
            return new Request(int.Parse(LineArray[0]),
                LineArray[1],
                int.Parse(LineArray[2]),
                int.Parse(LineArray[3]),
                DateTime.Parse(LineArray[4]));
        }
    }
}
