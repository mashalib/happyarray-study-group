﻿using System;


namespace HW4
{
    public class Person
    {
        protected int age;

        public int Age
        {
            get { return age; }
            set { age = value; }
        }


        public void Greet()
        {
            Console.WriteLine("Hello!");
        }
        public int SetAge(int n)
        {
            age = n;
            return age;
        }
    }
    public class Student : Person
    {
        public void GoToClasses()
        {
            Console.WriteLine("I'm going to class");
        }
        public void ShowAge()
        {
            Console.WriteLine($"My age is: {Age} years old.");
        }
    }

    
    class Teacher:Person
    {
        private string subject;

        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }
        public void Explain ()
        {
            Console.WriteLine("Explanation begins");
        }

       
    } 
   
}
