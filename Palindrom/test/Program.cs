﻿using System;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        
            {
                string continRequest = "yes";

                while (continRequest == "yes")
                {
                    //text input
                    Console.Clear();
                    Console.WriteLine("Wellcome to Text Helper.\n\n" +
                        "Please type or copy your text and press ENTER:\n");
                    TextHelper input = new TextHelper(Console.ReadLine());



                    //word count
                    int countWords = input.GetWordsCount(input.Text);
                    Console.WriteLine($"\nThis text contains {countWords} words.");

                    //word count
                    int countLetters = input.GetLettersCount(input.Text);
                    Console.WriteLine($"\nThis text contains {countLetters} letters.");

                    //palindrome selector
                    string[] palindroms = input.GetPalindromes(input.Text);
                    Console.Write($"\nThis text contains {palindroms.Length} palindrome word(s):\n");
                    for (int i = 0; i < palindroms.Length; i++)
                    {
                        Console.WriteLine($"{palindroms[i]}");
                    }

                    //specific word count
                    Console.Write("\nWhat word would you like to count in your text?\t");
                    string specWord = Console.ReadLine();
                    int countSpecWord = input.GetSpecificWords(input.Text, specWord);
                    Console.WriteLine($"\nWord \"{specWord}\" appears in your text {countSpecWord} time(s).\n");



                    //continue request
                    Console.WriteLine("\n\nWould you like to check one more text? (Type yes or no)");
                    continRequest = Console.ReadLine();
                    if ((continRequest.ToLower() != "yes") && (continRequest!.ToLower() != "no"))
                    {
                        Console.WriteLine("Please try again.\n" +
                            "Would you like to check one more text? (Type yes or no)");
                        continRequest = Console.ReadLine();
                    }
                    else if (continRequest!.ToLower() == "no")
                    {
                        Console.WriteLine("Thank you for using Text Helper. \n Press any key to close this window");
                    }
                }

            }
        }
    }