﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Palindrom
{
    public class TextHelper
    {
        public string Text; //property for text input

        public TextHelper(string textInput) // constructor, text input required
        {
            Text = textInput;
        }

        //methods
        public int GetWordsCount(string text)//numbers counts as words
        {
            int counter = 0;
            text = text.Trim(' ');
            string[] words = text.Split(' ');

            foreach (var word in words)
            {
                if (word != " ")
                {
                    counter++;
                }
            }
            return counter;
        }

        public int GetSpecificWords(string text, string specWord)
        {
            int counter = 0;
            text = text.Trim(' ');
            string[] words = text.Split(' ');

            foreach (var word in words)
            {
                string checkWord = word.Trim(',', '.', '!', '?', ':', ';', '-');
                if (checkWord.ToLower() == specWord.ToLower())
                {
                    counter++;
                }
            }
            return counter;
        }

        public int GetLettersCount(string text)//numbers counts as letters
        {
            int counter = 0;
            foreach (char c in text)
            {
                if ((c != ' ') & (c != '.') & (c != ',')
                    & (c != '!') & (c != '?') & (c != ';')
                    & (c != ':') & (c != '-'))
                {
                    counter++;
                }
            }

            return counter;
        }

        public string[] GetPalindromes(string text)
        {
            int counter = 0;
            text = text.Trim(' ');
            string[] words = text.Split(' ');
            string[] testPalindromes = new string[words.Length];
            foreach (var word in words)
            {
                string originalOrder = word.Trim(',', '.', '!', '?', ':', ';', '-');
                string reverseOrder = "";

                if (originalOrder != " ")
                {
                    for (int i = (originalOrder.Length - 1); i >= 0; i--)
                    {
                        reverseOrder += originalOrder[i];
                    }

                    if (originalOrder.ToLower() == reverseOrder.ToLower())
                    {
                        testPalindromes[counter] = originalOrder;
                        counter++;
                    }
                }
            }
            //delete empty members
            string[] palindromes = new string[counter];
            for (int i = 0; i < counter; i++)
            {
                palindromes[i] = testPalindromes[i];
            }
            return palindromes;

        }
    }
}
