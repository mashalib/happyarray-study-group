﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace HM_11
{
    class Program
    {
        static void Main(string[] args)
        {

            string input = Console.ReadLine();
            Stack<char> lettersResult = new Stack<char>();
            lettersResult.Push(input[0]);

            for (int i = 1; i < input.Length; i++)
            {
                if (input[i] == lettersResult.Peek())
                {
                    lettersResult.Pop();
                }
                else
                {
                    lettersResult.Push(input[i]);
                }

            }
            string result = "";
            foreach (var item in lettersResult)
            {
                result = item + result;
            }
            Console.Write(result);
        }
    }
}
