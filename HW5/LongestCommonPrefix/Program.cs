﻿using System;
using System.Collections.Generic;

namespace LongestCommonPrefix
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] input;
            List<string> words = new List<string>();
            string longestPrefix = "";

            Console.WriteLine
                ($"Enter your list of words\n(please use only lowercase leters and one space as word separator): ");
            input = Console.ReadLine().Split(" ");
            Console.WriteLine(FindLongestPrefix(input));


             string FindLongestPrefix(string [] input)
            {
                foreach (var item in input)
                {
                    words.Add(item);
                }

                if (words.Count == 1)// only one word in input
                {
                    return words[0];
                }

                else
                {

                    for (int letter = 0; letter < words[0].Length; letter++)
                    {
                        string firstWord = words[0];

                        for (int wordNumber = 1; wordNumber < words.Count; wordNumber++)
                        {
                            string testWord = words[wordNumber];
                            if ((letter == words[wordNumber].Length) || (testWord[letter] != firstWord[letter]))
                            {
                                longestPrefix = words[0].Substring(0, letter);
                                return longestPrefix;

                            }
                            
                        }

                    }

                }
                return words[0];
            }
        }
    }
}
