﻿using System;
using System.Collections.Generic;

namespace hw5
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] input;
            List<int> intInput = new List<int>();
            int target;


            Console.WriteLine("Enter an array of integers (x,y,...,n):");
            input = Console.ReadLine().Split(",");
            foreach (var item in input)
            {
                intInput.Add(int.Parse(item));
            }
            Console.WriteLine("Enter a target:");
            target = int.Parse(Console.ReadLine());

            for (int i = 0; i < intInput.Count; i++)
            {
                int temp;
                temp = target - intInput[i];
                if ((intInput.Contains(temp)) && (intInput.IndexOf(temp) != i))
                {
                    Console.WriteLine(i + "," + intInput.IndexOf(temp));
                    break;
                }
            }

        }
    }
}
