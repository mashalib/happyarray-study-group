﻿using System;
using System.Collections.Generic;

namespace _19_braces
{
    class Program
    {
        static void Main(string[] args)
        {

            string result = "";
            Console.WriteLine("enter number of cases");
            int numberOfCases = Convert.ToUInt16(Console.ReadLine());
            Console.WriteLine("enter testcases");
            for (int i = 0; i < numberOfCases; i++)
            {
                string input = Console.ReadLine();
                var braces = new List<string>();
                bool isBracesOverlap = false;

                foreach (var letter in input)
                {
                    switch (letter)
                    {
                        case '(':
                            braces.Add("(");
                            break;
                        case ')':
                            if (braces[braces.Count - 1] == "(")
                            {
                                braces.RemoveAt(braces.Count - 1);
                            }
                            else
                            {
                                isBracesOverlap = true;
                            }
                            break;
                        case '{':
                            braces.Add("{");
                            break;
                        case '}':
                            if (braces[braces.Count - 1] == "{")
                            {
                                braces.RemoveAt(braces.Count - 1);
                            }
                            else
                            {
                                isBracesOverlap = true;
                            }
                            break;
                        case '[':
                            braces.Add("[");
                            break;
                        case ']':
                            if (braces[braces.Count - 1] == "[")
                            {
                                braces.RemoveAt(braces.Count - 1); 
                            }
                            else
                            {
                                isBracesOverlap = true;
                            }
                            break;
                        case '<':
                            braces.Add("<");
                            break;
                        case '>':
                            if (braces[braces.Count - 1] == "<")
                            {
                                braces.RemoveAt(braces.Count - 1);
                            }
                            else
                            {
                                isBracesOverlap = true;
                            }
                            break;
                        default:
                            break;
                    }
                }
                if ((isBracesOverlap == false) && (braces.Count == 0))
                {
                    result = result + "1 ";
                }
                else
                {
                    result = result + "0 ";
                }
            }
            Console.WriteLine(result);
        }

    }


}

