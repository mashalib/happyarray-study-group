﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw9
{

    public class Person
    {
        public Person()
        {
        }

        public Person(string name, int age, string city, bool isEmploee)
        {
            Name = name;
            Age = age;
            City = city;
            IsEmploee = isEmploee;
        }

        internal object ToUpper()
        {
            throw new NotImplementedException();
        }

        public string Name { get; set; }
        public int Age { get; set; }
        public string City { get; set; }
        public bool IsEmploee { get; set; }

        public List<Person> GetList()
        {

            var people = new List<Person>
            {
                new Person ("Tom", 23, "Minks",false),
                new Person ("Bob", 27, "Moscow", true),
                new Person ("Sam", 29, "King of Prusia", true),
                new Person ("Alice", 24, "Kiev", false),
                new Person ("Fon", 55, "Gili", false),
                new Person ("Ron", 71, "Dalas", false),
                new Person ("Hohe", 23, "New York", false),
                new Person ("Shon", 13, "Kiev", false),
                new Person ("Mark", 41, "Sluck", true),
                new Person ("Nove", 32, "Phily", false),
                new Person ("Lolin", 18, "Stambul", true),
                new Person ("Spenser", 15, "Wayn", true)

            };
            return people;
        }
    }

}
