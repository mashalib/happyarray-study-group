﻿using System;
using System.Collections.Generic;


namespace oceanView
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter heights of the buildings in the line separated by space");
            string[] input = Console.ReadLine().Split(' ');

            int[] inputNumbers = Array.ConvertAll(input, p => int.Parse(p));

            string result = "";
            Stack<int> oceanViews = new Stack<int>();
            oceanViews.Push(inputNumbers.Length - 1);
            for (int i = (inputNumbers.Length - 2); i >= 0; i--)
            {
                if (inputNumbers[i]>inputNumbers[oceanViews.Peek()])
                {
                    oceanViews.Push(i);
                }
            }
            foreach (var item in oceanViews)
            {
                result = result + " "+ Convert.ToString(item);
            }
            
            Console.WriteLine("Indices (0-indexed) of buildings that have an ocean view:" + result);

        }
        
    }
}
