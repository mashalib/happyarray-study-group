﻿using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            double firstNumber = 0;
            double secondNumber = 0;
            string opertr;

            while (true)
            {
                try
                {

                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.WriteLine("\t\t\t\t  Calculator\n\t\t\t----------------------------\n\n\n");
                    
                    Console.Write("Enter first number:\t\t\t");
                    firstNumber = Convert.ToDouble(Console.ReadLine());

                    Console.Write("Select operator: +, -, *, or /\t\t");
                    opertr = Console.ReadLine();
                    if ((opertr != "+")&&(opertr != "-")&&(opertr != "*")&&(opertr != "/"))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Unknown operator. Please enter +, -, *, or /");
                    }


                    else
                    {
                        Console.Write("Enter second number:\t\t\t");
                        secondNumber = Convert.ToDouble(Console.ReadLine());

                        if (opertr == "+")
                        {
                            Console.WriteLine("\nAnswer\t\t\t\t\t=\n\t\t\t\t\t" + (firstNumber + secondNumber));
                        }
                        else if (opertr == "-")
                        {
                            Console.WriteLine("\nAnswer\t\t\t\t\t=\n\t\t\t\t\t" + (firstNumber - secondNumber));
                        }
                        else if (opertr == "*")
                        {
                            Console.WriteLine("\nAnswer\t\t\t\t\t=\n\t\t\t\t\t" + (firstNumber * secondNumber));
                        }
                        else if (opertr == "/")
                        {
                            if (secondNumber == 0)
                                Console.WriteLine("\nAnswer\t\t\t\t\t=\n\t\t\t\t\t" + (0));
                            else

                                Console.WriteLine("\nAnswer\t\t\t\t\t=\n\t\t\t\t\t" + (firstNumber / secondNumber));
                        }
                       

                    }
                }
                catch (Exception ex)
            {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(ex.Message +"\nPlease enter a number"); ;
            }

                
                Console.ReadKey();
            }
    }
    }
}
