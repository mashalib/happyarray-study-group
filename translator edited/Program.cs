﻿using Google.Cloud.Translation.V2;
using System;
using System.Text;

namespace translator_edited
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Console.InputEncoding = Encoding.Unicode;
            
            
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", @"key.json");

            var client = TranslationClient.Create();
            foreach (var language in client.ListLanguages(LanguageCodes.English))
            {
                Console.WriteLine($"{language.Code}\t{language.Name}");

            }
            //Console.ReadKey();
            //Console.Clear();
            Console.WriteLine("Enter your text:");
            var myText = Console.ReadLine();

            var detectedLanguage = client.DetectLanguage(myText).Language;
            Console.WriteLine(detectedLanguage);

            Console.WriteLine("What language to translate: ");

            var translateLanguage = Console.ReadLine();

            var translatedText = client.TranslateText(myText, translateLanguage, detectedLanguage).TranslatedText;
            Console.WriteLine(translatedText);

            Console.ReadKey();
        }
    }
}
