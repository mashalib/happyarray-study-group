﻿using System;

namespace fibonachi
{
    class Program
    {
        static void Main(string[] args)
        {
            string status = "yes";
            while (status == "yes")
            {
                Console.Write(  "enter your number:"); 
                int n = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Solved using method #1: " + Fibonachi1(n));
                Console.WriteLine("Solved using method #2: " + Fibonachi2(n));

                Console.WriteLine("again? (yes/no)");
                status = Console.ReadLine();
                
            }
        }

        static int Fibonachi1(int n)
        {
            if (n == 0 || n == 1) return n;
            return Fibonachi1(n - 1) + Fibonachi1(n - 2);
        }

        static int Fibonachi2(int n)
        {
            int result = 0; 
            int b = 1; 
            int temp;
            for (int i = 0; i < n; i++)
            {
                temp = result;
                result = b;
                b += temp;
            }
            return result;

        }
    }
}
