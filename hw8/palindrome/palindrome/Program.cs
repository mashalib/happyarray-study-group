﻿
using System;

namespace palindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your number");
            int number = Convert.ToInt32(Console.ReadLine());
            int temp = number;
            int reverseNumber = 0, remainder=0;
            while (temp > 0)
            {
                remainder = temp % 10;
                temp = temp / 10;
                reverseNumber = reverseNumber * 10 + remainder;
            }
            if (number==reverseNumber)
            {
                Console.WriteLine($"{number} is palindrome");
            }
            else
            {
                Console.WriteLine($"{number} is not palindrome");
            }
        }
    }
}
