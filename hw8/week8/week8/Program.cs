﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace week8
{
    class Program
    {
        static void Main(string[] args)
        {
            Task[] tasks = new Task[10];

            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = new Task(Method);
                tasks[i].Start();
                
            }
            Task.WaitAll(tasks);

        }

        public static void Method()
        {
            
                Console.WriteLine("my name is Masha");
                Thread.Sleep(2000);
           
        }
    }
}
