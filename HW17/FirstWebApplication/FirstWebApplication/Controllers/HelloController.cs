﻿using Microsoft.AspNetCore.Mvc;

namespace FirstWebApplication.Controllers
{
    public class HelloController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.MyBag = "Hello world"; 
                return View();
        }
    }
}
