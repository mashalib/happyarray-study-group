﻿using Microsoft.AspNetCore.Mvc;

namespace homeworkWebApp.Controllers
{
    public class LastController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.MyBag = "First page";
            return RedirectToAction("Index1");
        }
        
        [HttpGet]
        public async Task Index1()
        {
            string content = @"<form method='post'>
                <label>What's your mood today?:</label><br />
                <input name='mood' /><br />
                <label>What part of the day is right now?:</label><br />
                <input  name='day' /><br />
                <input type='submit' value='Send' />
            </form>";
            Response.ContentType = "text/html;text/html";
            await Response.WriteAsync(content);
        }
        [HttpPost]
        public string Index1(string mood, string day) => $"It is a {mood} {day}, isn't it?";
    }
}
