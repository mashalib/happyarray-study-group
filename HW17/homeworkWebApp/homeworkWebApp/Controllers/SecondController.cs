﻿using Microsoft.AspNetCore.Mvc;

namespace homeworkWebApp.Controllers
{
    public class SecondController : Controller
    {
        public IActionResult Index()
        {
            //ViewBag.MyBag = "Second page";
            //return View();
            return Redirect("http://localhost:5193/First");
        }
    }
}
