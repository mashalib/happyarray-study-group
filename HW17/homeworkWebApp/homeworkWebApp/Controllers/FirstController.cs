﻿using Microsoft.AspNetCore.Mvc;

namespace homeworkWebApp.Controllers
{
    public class FirstController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.MyBag = "First page";
            return View();
        }
    }
}
