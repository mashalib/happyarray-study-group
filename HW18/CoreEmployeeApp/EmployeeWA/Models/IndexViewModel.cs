﻿using EmployeeWA.Repo;

namespace EmployeeWA.Models
{
    public class IndexViewModel
    {
        public IEnumerable<Employee> Employees { get; }
        public PageViewModel PageViewModel { get; }
        public IndexViewModel(IEnumerable<Employee> employees, PageViewModel viewModel)
        {
            Employees = employees;
            PageViewModel = viewModel;
        }
    }
}
