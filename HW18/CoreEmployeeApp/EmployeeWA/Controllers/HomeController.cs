﻿using EmployeeWA.Models;
using EmployeeWA.Repo;
using Microsoft.AspNetCore.Mvc;
using System.Data.Entity;
using System.Diagnostics;

namespace EmployeeWA.Controllers
{
    public class HomeController : Controller
    {
        
        private readonly IEmployeeRepository _employeeRepository;

        public HomeController(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        // *****
        public ActionResult Create()
        {
            ViewBag.Message = "Enter information of new employee:";

            return View();
        }

        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                _employeeRepository.Create<Employee>(employee);
                _employeeRepository.Save();
                return RedirectToAction("List");
            }
            return View(employee);
        }
        [HttpGet]
        public ActionResult EditEmployee(int Id)
        {
            
            Employee employee = _employeeRepository.GetById(Id);
            return View(employee);
        }

        [HttpPost]
        public ActionResult EditEmployee(Employee employee)
        {
            
            if (ModelState.IsValid)
            {
                _employeeRepository.Update<Employee>(employee);
                _employeeRepository.Save();
                return RedirectToAction("List");
            }
            return View(employee);
        }

        [HttpGet]
        public ActionResult Delete(int Id)
        {
            Employee e = _employeeRepository.GetById(Id);
            return View(e);
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(int Id)
        {

            _employeeRepository.Delete(Id);
            _employeeRepository.Save();
            return RedirectToAction("List", "Home");


        }
        
        protected override void Dispose(bool disposing)
        {
            _employeeRepository.Dispose();
            base.Dispose(disposing);
        }

        

        public async Task<IActionResult> List(SortState sortOrder = SortState.FirtsNameAsc, string searchString = "",int page = 1)
        {
            int pageSize = 5;

            IQueryable<Employee>? employees = (IQueryable<Employee>?)_employeeRepository.GetAll();


            ViewData["FirstNameSort"] = sortOrder == SortState.FirtsNameAsc ? SortState.FirtsNameDesc : SortState.FirtsNameAsc;
            ViewData["LastNameSort"] = sortOrder == SortState.LastNameAsc ? SortState.LastNameDesc : SortState.LastNameAsc;
            ViewData["DepartmentNameSort"] = sortOrder == SortState.DepartmentNameAsc ? SortState.DepartmentNameDesc : SortState.DepartmentNameAsc;
            ViewData["CurrentFilter"] = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                employees = employees.Where(s => s.LastName.Contains(searchString) ||
                                                s.FirstName.Contains(searchString) ||
                                                s.DepartmentName.Contains(searchString));
            }
            employees = sortOrder switch
            {
                SortState.FirtsNameDesc => employees.OrderByDescending(s => s.FirstName),
                SortState.LastNameAsc => employees.OrderBy(s => s.LastName),
                SortState.LastNameDesc => employees.OrderByDescending(s => s.LastName),
                SortState.DepartmentNameAsc => employees.OrderBy(s => s.DepartmentName),
                SortState.DepartmentNameDesc => employees.OrderByDescending(s => s.DepartmentName),
                _ => employees.OrderBy(s => s.FirstName),
            };

            var count = await employees.CountAsync();
            var items = await employees.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel(items, pageViewModel);

            return View(viewModel);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}