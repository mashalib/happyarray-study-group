﻿namespace EmployeeWA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmployeesDB3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Employees", "WebUrl", c => c.String(nullable: false));
            AlterColumn("dbo.Employees", "PhoneNumber", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Employees", "PhoneNumber", c => c.String());
            AlterColumn("dbo.Employees", "WebUrl", c => c.String());
        }
    }
}
