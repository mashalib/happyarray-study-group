﻿namespace EmployeeWA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmployeesDB : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "LastName", c => c.String());
            AddColumn("dbo.Employees", "DepartmentName", c => c.String());
            AddColumn("dbo.Employees", "Basic", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Employees", "DA", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Employees", "HRA", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Employees", "NetSalary", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "NetSalary");
            DropColumn("dbo.Employees", "HRA");
            DropColumn("dbo.Employees", "DA");
            DropColumn("dbo.Employees", "Basic");
            DropColumn("dbo.Employees", "DepartmentName");
            DropColumn("dbo.Employees", "LastName");
        }
    }
}
