﻿namespace EmployeeWA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmployeesDB8 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Employees", "Email", c => c.String());
            AlterColumn("dbo.Employees", "WebUrl", c => c.String());
            AlterColumn("dbo.Employees", "PhoneNumber", c => c.String());

          
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Employees", "PhoneNumber", c => c.String(nullable: false));
            AlterColumn("dbo.Employees", "WebUrl", c => c.String(nullable: false));
            AlterColumn("dbo.Employees", "Email", c => c.String(nullable: false));
        }
    }
}
