﻿namespace EmployeeWA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmployeesDB1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "Email", c => c.String());
            AddColumn("dbo.Employees", "WebUrl", c => c.String());
            AddColumn("dbo.Employees", "PhoneNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "PhoneNumber");
            DropColumn("dbo.Employees", "WebUrl");
            DropColumn("dbo.Employees", "Email");
        }
    }
}
