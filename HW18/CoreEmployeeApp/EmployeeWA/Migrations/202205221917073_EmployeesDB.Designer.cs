﻿// <auto-generated />
namespace EmployeeWA.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class EmployeesDB : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(EmployeesDB));
        
        string IMigrationMetadata.Id
        {
            get { return "202205221917073_EmployeesDB"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
