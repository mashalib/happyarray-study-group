﻿using System.Data.Entity;

namespace EmployeeWA.Repo
{
    public interface IEmployeeContext
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        void SetModified(object entity);
        void SetAdded(object entity);
        void SetDeleted(object entity);

       
        int SaveChanges();
        void Dispose();
    }
}
