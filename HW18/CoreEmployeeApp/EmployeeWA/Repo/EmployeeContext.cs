﻿using System.Data.Entity;

namespace EmployeeWA.Repo
{
    public class EmployeeContext : DbContext, IEmployeeContext
    {
        public EmployeeContext() : base("Data Source = MASHA; Initial Catalog = Employees; Integrated Security = True")
        { }
        public DbSet<Employee> Employees { get; set; }

        //public static EmployeeContext Create()
        //{
        //    return new EmployeeContext();
        //}

        public void SetModified(object entity)
        {
            Entry(entity).State=EntityState.Modified;
        }
        public void SetAdded(object entity)
        {
            Entry(entity).State = EntityState.Added; 
        }

        public void SetDeleted(object entity)
        {
            Entry(entity).State = EntityState.Deleted ;
        }

        
    }
}
