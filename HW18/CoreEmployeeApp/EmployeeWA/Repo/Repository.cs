﻿namespace EmployeeWA.Repo
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly IEmployeeContext _employeeContext;
        public Repository(IEmployeeContext employeeContext)
        {
            _employeeContext = employeeContext;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _employeeContext.Set<TEntity>();
        }

        public virtual TEntity GetById<T>(T Id)
        {
            return _employeeContext.Set<TEntity>().Find(Id);
        }

        public void Create<T>(TEntity entity)
        {
            _employeeContext.SetAdded(entity);
        }

        public void Update<T>(TEntity entity)
        {
            _employeeContext.SetModified(entity);

        }

        public void Delete<T>(T Id)
        {
            TEntity entity = _employeeContext.Set<TEntity>().Find(Id);
            _employeeContext.SetDeleted(entity); 
        }

        public void Save()
        {
            _employeeContext.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _employeeContext.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}

