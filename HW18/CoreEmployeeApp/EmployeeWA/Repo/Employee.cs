using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EmployeeWA.Repo
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("First Name")]
        [Required(ErrorMessage = "Please enter employee's first name")]
        [StringLength(15,MinimumLength =3,ErrorMessage ="First name should contain at least 3 characters and not exceed 15 ")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required(ErrorMessage = "Please enter employee's last name")]
        [StringLength(25, MinimumLength = 3, ErrorMessage = "Last name should contain at least 3 characters and not exceed 25 ")]
        public string LastName { get; set; }

        [DisplayName("Department Name")]
        [Required]
        public string DepartmentName { get; set; }

        [Range(0,1000000,ErrorMessage ="Out of Range")]
        public decimal Basic { get; set; }
        [Range(0, 1000000, ErrorMessage = "Out of Range")]
        public decimal DA { get; set; }
        [Range(0, 1000000, ErrorMessage = "Out of Range")]
        public decimal HRA { get; set; }

        [DisplayName("Net Salary")]
        [Range(0, 1000000, ErrorMessage = "Out of Range")]
        public decimal NetSalary { get; set; }

        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is invalid")]
        public string Email { get; set; }
      
        [DisplayName("Web Url")]
        [RegularExpression(@"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$", ErrorMessage = "Web Url is invalid")]
        public string WebUrl { get; set; }

        [DisplayName("Phone Number")]
        [RegularExpression(@"\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})", ErrorMessage = "Phone number is invalid. Phone number format:xxx-xxx-xxxx")]
        public string PhoneNumber { get; set; }
    }
}
