﻿namespace EmployeeWA.Repo
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById<T>(T Id);
        void Update<T>(TEntity entity);
        void Create<T>(TEntity entity);
        void Delete<T>(T Id);
        void Save();
        void Dispose();
    }
}
