﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeesWebApp.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private EmployeeContext db;

        public Repository()
        {
            this.db = new EmployeeContext();
        }


        public IEnumerable<TEntity> GetAll()
        {
            return db.Set<TEntity>();
        }

        public virtual TEntity GetById<T>(T Id)
        {
            return db.Set<TEntity>().Find(Id);
        }

        public void Create<T>(TEntity entity)
        {
            db.Entry(entity).State = System.Data.EntityState.Added;
        }

        public void Update<T>(TEntity entity)
        {
            db.Entry(entity).State = System.Data.EntityState.Modified;

        }

        public void Delete<T>(T Id)
        {
            TEntity entity = db.Set<TEntity>().Find(Id);
            db.Entry(entity).State = System.Data.EntityState.Deleted;
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
