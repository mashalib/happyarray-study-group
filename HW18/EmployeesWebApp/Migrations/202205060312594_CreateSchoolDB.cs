namespace EmployeesWebApp.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class CreateSchoolDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DepartmentName = c.String(),
                        Basic = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DA = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HRA = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NetSalary = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("Employees");
        }
    }
}
