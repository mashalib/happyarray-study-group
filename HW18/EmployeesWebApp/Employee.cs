﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace EmployeesWebApp
{
    public partial class Employee
    {
        public int Id { get; set;}
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")] 
        public string LastName { get; set; }

        [DisplayName("Department Name")]
        public string DepartmentName { get; set; }
        public decimal Basic { get; set; }
        public decimal DA { get; set; }
        public decimal HRA { get; set; }
        
        [DisplayName("Net Salary")]
        public decimal NetSalary { get; set; }



    }
}