﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EmployeesWebApp
{
    public class EmployeeContext : DbContext
    {
        
            public EmployeeContext() : base(@"Data Source=MININT-LF8N7L3\SQLEXPRESS;Initial Catalog=employee;Integrated Security=True")
            { }

            public DbSet<Employee> Employees { get; set; }
            
        
    }
}