﻿using EmployeesWebApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmployeesWebApp.Controllers
{
    public class HomeController : Controller
    {


        IRepository<Employee> db;

        public HomeController()
        {
            db = new EmployeeRepository();
        }

        public ActionResult Index()
        {
            return View(db.GetAll());
        }

        public ActionResult Create()
        {
            ViewBag.Message = "Enter information of new employee:";

            return View();
        }

        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Create<Employee>(employee);
                db.Save();
                return RedirectToAction("List");
            }
            return View(employee);
        }
        [HttpGet]
        public ActionResult EditEmployee(int Id)
        {
            ViewBag.Message = "Enter name of employee which data you would like to update:";
            Employee employee = db.GetById(Id);
            return View(employee);
        }

        [HttpPost]
        public ActionResult EditEmployee(Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Update<Employee>(employee);
                db.Save();
                return RedirectToAction("List");
            }
            return View(employee);
        }

        [HttpGet]
        public ActionResult Delete(int Id)
        {
            Employee e = db.GetById(Id);
            return View(e);
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(int Id)
        {

            db.Delete(Id);
            db.Save();
            return RedirectToAction("List", "Home");


        }
        public ActionResult List()
        {
            ViewBag.Message = "If you would like to delete an employee click Delete. " +
                "If you would like to edit information about an employee click Update";

            return View(db.GetAll());
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}